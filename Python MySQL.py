#!/usr/bin/python3
import argparse
import configparser
import os

# try:
#    import tomllib
# except ModuleNotFoundError:
#    import tomli as tomllib
import pymysql  # apt install python3-pymysql


class Application:
    # def read_config( self , path ):
    #     config = configparser.ConfigParser()
    #     path = os.path.normpath( path )
    #     if( os.path.exists( path ) ):
    #         config.read( path )
    #     return config
    # def read_toml_config(config_path):
    #    with open(config_path, "rb") as config_file:
    #        return tomllib.load(config_file)

    def get_mysql_connection(self, mysql_config_file, mysql_profile):
        config = configparser.ConfigParser()
        config.read(os.path.expanduser(mysql_config_file))

        connection = pymysql.connect(
            host=config[mysql_profile].get("host"),
            database=config[mysql_profile].get("database"),
            user=config[mysql_profile].get("user"),
            password=config[mysql_profile].get("password"),
            cursorclass=pymysql.cursors.DictCursor,
        )
        return connection

    def __init__(self):
        parser = argparse.ArgumentParser(
            description="",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        # parser.add_argument( '--verbose' , '-v' , help='Verbose output' , action='store_true')
        # parser.add_argument( '--dry-run' , '-n' , help='Do not perform any changes' , action='store_true' )
        # parser.add_argument(
        #                     '-c' ,
        #                     '--config-file' ,
        #                     help = 'Configuration file',
        #                     default = os.path.join( os.path.expanduser('~') , '.config' , 'REPLACE_FILENAME.conf' )
        #                   )
        parser.add_argument(
            "-c",
            "--mysql-config-file",
            help="MySQL configuration file",
            default=os.path.join(os.path.expanduser("~"), ".my.cnf"),
        )
        parser.add_argument(
            "--mysql-profile",
            help="Profile used for MySQL configuration",
            default="clientreadonly",
        )
        args = parser.parse_args()

        # self.config = self.read_config ( args.config_file )
        connection = self.get_mysql_connection(
            args.mysql_config_file, args.mysql_profile
        )
        SQL = """
          SELECT "data";
        """
        with connection.cursor() as cursor:
            cursor.execute(SQL)
            print(f"{cursor.fetchall()}")
        # commit doesn't occur by default for updates
        connection.commit()


if __name__ == "__main__":
    app = Application()
