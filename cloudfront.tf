terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "us-east-1"
}

resource "aws_cloudfront_distribution" "example_com" {
  enabled = true
  
  viewer_certificate {
    acm_certificate_arn = ""
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1.2_2021"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    allowed_methods  = [
      "GET",
      "HEAD",
    ]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = ""

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
    compress               = true
    viewer_protocol_policy = "https-only"
  }

  origin {
    connection_attempts = 3
    connection_timeout = 10
    domain_name = "example.com"
    origin_id   = "example.com"

    custom_origin_config {
      http_port = 80
      https_port = 443
      origin_keepalive_timeout = 5
      origin_protocol_policy = "https-only"
      origin_read_timeout = 30
      origin_ssl_protocols = [
      ]
    }
  }

}
