terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "4.22.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

resource "cloudflare_record" "example_com_CNAME" {
  zone_id = "<id in overview page>"
  name    = "example.com"
  type    = "CNAME"
  ttl     = 3600
  value   = "destination.example.com"
}

